import json
import xml.etree.ElementTree as et
from click import progressbar
import pandas as pd
import spacy
from nltk.stem.snowball import FrenchStemmer
from .veronis_stop_words import veronis_sw

spacy_fr = spacy.load("fr_core_news_sm")


def tokenize(text):
    return [tok.text for tok in spacy_fr.tokenizer(text)]


def xml_to_json(filepath, output_filepath, is_test: bool = False):
    et.XMLParser(encoding="utf-8")
    tree = et.parse(filepath)
    root = tree.getroot()
    data = []
    tokens = {
        "commentaire": "comment",
        "review_id": "review_id",
        "note": "note"
    }
    with progressbar(list(root)) as bar:
        for comment in bar:
            com_dict = {}
            for info in list(comment):
                if info.tag == "note" and info.text:
                    com_dict[tokens[info.tag]] = float(info.text.replace(",", "."))
                elif info.tag == "commentaire":
                    if info.text is None:
                        info.text = ""
                    com_dict[tokens[info.tag]] = info.text
                elif info.tag == "review_id":
                    com_dict[tokens[info.tag]] = info.text
            if is_test:
                com_dict[tokens["note"]] = ""
            data.append(com_dict)

    with open(output_filepath, 'w') as outfile:
        json.dump(data, outfile, ensure_ascii=False)


def read_xml_with_pandas(filepath):
    tree = et.parse(filepath)
    root = tree.getroot()
    columns = ["review_id", "note", "comment"]
    df = pd.DataFrame(columns=columns)
    for node in root:
        review_id = node.find("review_id").text if node is not None else ""
        note = node.find("note").text if node is not None else ""
        comment = node.find("commentaire").text if node is not None else ""
        df = df.append(pd.Series([review_id, note, comment], index=columns), ignore_index=True)
    print(df)


def read_json_to_dataframe(filepath):
    df = pd.read_json(filepath)
    return df


def preprocess_dataframe(df):
    df['comment'] = df['comment'].str.replace("[^\w\s']", "")
    df['comment'] = df['comment'].apply(lambda x: " ".join(x for x in x.split() if x not in veronis_sw))
    st = FrenchStemmer()
    df['comment'] = df['comment'].apply(lambda x: " ".join([st.stem(word) for word in x.split()]))
    return df


def tokenize_dataframe(df):
    df["comment"].apply(tokenize)
    return df


def write_dataframe_to_json(df, output_filepath):
    df.to_json(output_filepath, orient="records", force_ascii=False)

