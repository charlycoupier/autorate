import xml.etree.ElementTree as et
from click import progressbar
from util.preprocess_data import process_comment_simple


def get_value(comment: str, tag: str):
    for info in list(comment):
        if info.tag == tag:
            return info.text


def parse_xml(filepath: str):
    et.XMLParser(encoding="utf-8")
    tree = et.parse(filepath)
    root = tree.getroot()
    processed_comments_tokens = []
    tokens_dict = {}
    unique_tokens = []
    tokens_occur = []
    tokens_occur_per_comment = []
    tokens_dict_per_comment = {}
    i = 0
    with progressbar(list(root)) as bar:
        for comment in bar:
            comment_text = get_value(comment, "commentaire")
            if comment_text:
                comment_tokens = process_comment_simple(comment_text)
                processed_comments_tokens.append(comment_tokens)
                for token in comment_tokens:
                    if token not in unique_tokens:
                        # tokens_dict[token] = 1
                        unique_tokens.append(token)
                        tokens_occur.append(1)
                    else:
                        # tokens_dict[token] += 1
                        tokens_occur[unique_tokens.index(token)] += 1
                i += 1

    with open("../output/tokens_occur.txt", "w") as f:
        for index in range(len(unique_tokens)):
            f.write("{} : {}\n".format(unique_tokens[index], tokens_occur[index]))

    # with progressbar(processed_comments_tokens) as bar:
    #     for comment_tokens in bar:
    #         for key in tokens_dict:
    #             tokens_dict_per_comment[key] = 0
    #         # tokens_occur_per_comment.append([0] * len(tokens_dict))
    #         for token in comment_tokens:
    #             tokens_dict_per_comment[token] += 1
    #             # tokens_occur_per_comment[token] += 1
    # print(tokens_dict)
    # print(tokens_dict_per_comment)


if __name__ == '__main__':
    parse_xml("../corpus/dev.xml")





