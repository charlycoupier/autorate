import torch
from torch import device
from torchtext import data
import spacy

spacy_fr = spacy.load("fr_core_news_sm")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def tokenize(text):
    return [tok.vector for tok in spacy_fr(text)]


def main():
    # xml_to_json("corpus/train.xml", "corpus/train.json")
    # xml_to_json("corpus/dev.xml", "corpus/dev.json")
    # xml_to_json("corpus/test.xml", "corpus/test.json", True)

    COMMENT = data.Field(sequential=True, tokenize=tokenize, tokenizer_language="fr", lower=False, include_lengths=False, batch_first=True)
    NOTE = data.Field(sequential=False, use_vocab=False, dtype=torch.float)
    REVIEW_ID = data.Field(sequential=False, use_vocab=False)

    fields = {'comment': ('c', COMMENT), 'note': ('n', NOTE), 'review_id': ('r', REVIEW_ID)}

    train_dataset, dev_dataset, test_dataset = data.TabularDataset.splits(
        path='corpus',
        train='train.json',
        validation='dev.json',
        test='test.json',
        format='json',
        fields=fields
    )

    COMMENT.build_vocab(train_dataset)
    # NOTE.build_vocab(train_dataset)
    # REVIEW_ID.build_vocab(train_dataset)

    train_it, dev_it, test_it = data.BucketIterator.splits(
        (train_dataset, dev_dataset, test_dataset),
        batch_size=32,
        device=device,
        sort=False,
        sort_key=lambda x: x.review_id
    )

    # iterate over training
    for batch in train_it:
        print(batch)
        pass

    print("Hello")

    # train_data, test_data = datasets.IMDB.splits(TEXT, LABEL)

    # train_data, valid_data = train_data.split()

    # train, val, test = data.TabularDataset.splits(
    #     path='./data/', train='train.tsv',
    #     validation='val.tsv', test='test.tsv', format='tsv',
    #     fields=[('Text', TEXT), ('Label', LABEL)])


if __name__ == '__main__':
    main()

# class MLPDataset(Dataset):
#     def __init__(self):
#         self.comment = Tensor()
#         self.note = Tensor()
#         self.review_id = []
#
#     def __len__(self):
#         return len(self.comment)
#
#     def __getitem__(self, id):
#         return [self.comment[id], self.note[id]]
#
#
# class MLP(Module):
#     def __init__(self, n_inputs):
#         super(MLP, self).__init__()
#         self.layer = Linear(n_inputs, 1)
#         self.activation = Sigmoid()
#
#     def forward(self, X):
#         X = self.layer(X)
#         X = self.activation(X)
#         return X
#
#
# def create_dataset(root_xml, is_test: bool = False):
#     dataset = MLPDataset()
#     train, test = random_split(dataset, [[...], [...]])
#     # create a data loader for train and test sets
#     train_dl = DataLoader(train, batch_size=32, shuffle=True)
#     test_dl = DataLoader(test, batch_size=1024, shuffle=False)
#
#     dataset = MLPDataset()
#     with progressbar(list(root_xml)) as bar:
#         for elt in bar:
#             current_elt = {}
#             note_ok = False
#             comment_ok = False
#             review_id_ok = False
#             for info in list(elt):
#                 if not is_test and info.tag == "note":
#                     if info.text != "" and info.text is not None:
#                         note_ok = True
#                         current_elt["note"] = info.text
#                 elif info.tag == "commentaire":
#                     if info.text != "" and info.text is not None:
#                         comment_ok = True
#                         current_elt["comment"] = info.text
#                 elif info.tag == "review_id":
#                     if info.text != "" and info.text is not None:
#                         review_id_ok = True
#                         current_elt["review_id"] = info.text
#             if (is_test or note_ok) and comment_ok and review_id_ok:
#                 dataset.comment.append(current_elt["comment"])
#                 dataset.review_id.append(current_elt["review_id"])
#                 if not is_test:
#                     dataset.note.append(current_elt["note"])
#     return dataset
#
#
# def generate_evaluation_file(review_ids, predictions_list: list, max_iter):
#     dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
#     with open("evaluations/eval_tf_idf_test_{}_{}.txt".format(max_iter, dt_string), "w") as f:
#         for i, prediction in enumerate(predictions_list):
#             f.write("{} {}\n".format(review_ids[i], prediction))
#
#
# def main():
#     max_iter = 500
#     et.XMLParser(encoding="utf-8")
#     tree = et.parse('corpus/train.xml')
#     root = tree.getroot()
#     dataset = create_dataset(root)
#     print("train dataset created")
#
#     classifier = Pipeline([
#         ('vect', CountVectorizer(lowercase=False, stop_words=veronis_sw)),
#         ('tfidf', TfidfTransformer(use_idf=True)),
#         ('clsfier', svm.SVC(verbose=True, kernel='linear', max_iter=max_iter))
#     ])
#     classifier.fit(dataset.comment, dataset.note)
#     print("classifier trained")
#
#     test_tree = et.parse('corpus/test.xml')
#     test_root = test_tree.getroot()
#     dataset_test = create_dataset(test_root, True)
#     print("Dataset train OK, start prediction")
#
#     predicted_svm = classifier.predict(dataset_test.comment)
#
#     generate_evaluation_file(dataset.review_id, predicted_svm, max_iter)
#
#
# if __name__ == "__main__":
#     main()
