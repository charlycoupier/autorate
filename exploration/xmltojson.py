import json
import nltk
import xml.etree.ElementTree as et
from click import progressbar
from preprocess_data import process_comment

parser = et.XMLParser(encoding="utf-8")
tree = et.parse('corpus/train.xml')
root = tree.getroot()

data = []
tokens = {
    "commentaire": "commentaire",
    "movie": "movie_id",
    "review_id": "review_id",
    "user_id": "username",
    "note": "note"
}
i = 1

with progressbar(root.getchildren()) as bar:
    for comment in bar:
        data.append(
            json.dumps({"index":{"_index":"data_movies_2","_id":i}}, ensure_ascii=False)
        )
        com_dict = {}
        for info in comment.getchildren():
            if info.tag == "name":
                continue
            if info.tag == "note" and info.text:
                com_dict[tokens[info.tag]] = float(info.text.replace(",","."))
                continue
            com_dict[tokens[info.tag]] = info.text
            if info.tag == "commentaire" and info.text:
                com_dict["lst_mots"] = []
                clean_comment = process_comment(info.text, "stem")
                for word in clean_comment:
                    word_obj = {}
                    word_obj["text"] = word
                    com_dict["lst_mots"].append(word_obj)
        data.append(json.dumps(com_dict, ensure_ascii=False))
        i += 1

with open('mapping_train.json', 'a') as outfile:
    for elt in data:
        outfile.write(elt + "\n")



