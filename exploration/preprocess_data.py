import nltk
import re
from veronis_stop_words import veronis_sw
from nltk.tag.stanford import StanfordPOSTagger
from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer
from nltk.stem.snowball import FrenchStemmer

nltk.download('punkt')
pos_root_path="./stanford_tagger/"
pos_tagger = StanfordPOSTagger(
    pos_root_path + "models/french-ud.tagger",
    pos_root_path + "stanford-postagger.jar",encoding='utf8'
)
re_punct = re.compile(r"^\W+$")

def process_comment(comment:str, method="lemma"):
    words = nltk.tokenize.word_tokenize(comment)
    processed = []
    if method == "lemma":
        tag_words = pos_tagger.tag(words)
        lemmatizer = FrenchLefffLemmatizer()

        for tag_word in tag_words:
            word = tag_word[0]
            tag = tag_word[1]
            # re_punct = re.compile(r"^\W+$")
            if tag == "PUNCT":
                continue
            word = word.lower()
            if word in veronis_sw:
                continue
            if tag in ["ADJ"]:
                word = lemmatizer.lemmatize(word,'a')
            elif tag in ["VERB"]:
                word = lemmatizer.lemmatize(word,'v')
            elif tag in ["NOUN"]:
                word = lemmatizer.lemmatize(word,'n')
            elif tag in ["ADV"]:
                word = lemmatizer.lemmatize(word,'r')
            else:
                word = lemmatizer.lemmatize(word)
            processed.append(word)

    elif method == "stem":
        stemmer = FrenchStemmer()
        for word in words:
            if re_punct.match(word):
                continue
            word = word.lower()
            if word in veronis_sw:
                continue
            word = stemmer.stem(word)
            processed.append(word)
                
    return processed