const fs = require("fs");
const path = require("path");
const readline = require("readline")

let inputDir = "json";
let outputDir = "json/cleansplit";

if (process.argv.length > 2) {
    if (fs.existsSync(process.argv[2])) {
        inputDir = process.argv[2];
        outputDir = path.join(inputDir, "cleansplit");
    } else {
        console.log("Le dossier n'existe pas");
        return;
    }
} else {
    console.log("Il faut passer le dossier qui contient tous les fragments de fichier");
    return;
}


if (!fs.existsSync(outputDir)){
    fs.mkdirSync(outputDir);
}

async function cleanSplit() {
    let i = 0;
    let previousOutputPath;
    let files = fs.readdirSync(inputDir, {withFileTypes: true}).filter(file => file.isFile());
    let outputPath;
    for (let file of files) {
        let inputPath = path.join(inputDir, file.name)
        outputPath = path.join(outputDir, file.name);
        if (fs.existsSync(outputPath)) {
            fs.unlinkSync(outputPath);    
        }
        let inputStream = fs.createReadStream(inputPath);
        const rl = readline.createInterface({
            input: inputStream,
        });
        let j = 0;
        let test = false;
        for await (let line of rl) {
            if (previousOutputPath && j === 0 && !line.startsWith('{"index":') && !line.startsWith('{"movie_id":')) {
                fs.appendFileSync(previousOutputPath, line);
                test = true;
            } else if (previousOutputPath && j === 1 && line.startsWith('{"movie_id":')) {
                fs.appendFileSync(previousOutputPath, line);
                test = true;
            } else {
                if (j !== 0 && !test) {
                    fs.appendFileSync(outputPath, "\n");
                }
                fs.appendFileSync(outputPath, line);
                test = false;
            }
            ++j;
        }
        if (previousOutputPath) {
            fs.appendFileSync(previousOutputPath, "\n");
        }
        previousOutputPath = outputPath;
        ++i;
    }
    fs.appendFileSync(outputPath, "\n");
}

cleanSplit();