# Ajouter un index :
curl -X PUT "omegaserv.net:9200/data_movie" -H 'Content-Type: application/json' -d "@mapping_movie.json"
curl -X PUT "omegaserv.net:9200/data_movies" -H 'Content-Type: application/json' -d "@mapping_movie.json"

# Ajouter des données
curl -H --output /dev/null --write-out "%{http_code}\n" --silent 'Content-Type: application/x-ndjson' -XPOST 'omegaserv.net:9200/data_movie/_bulk?pretty' --data-binary @json/mapping_train.json
curl -H --output /dev/null --write-out "%{http_code}\n" --silent 'Content-Type: application/x-ndjson' -XPOST 'omegaserv.net:9200/data_movies/_bulk?pretty' --data-binary @json/mapping_train.json

# Supprimer des données
curl -XDELETE omegaserv.net:9200/data_movie
curl -XDELETE omegaserv.net:9200/data_movies