import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from datetime import datetime
import keras
import numpy
from tensorflow.python.keras.layers import LSTM, Dense, Dropout, Flatten, Conv1D, MaxPooling1D, SimpleRNN
from tensorflow.python.keras.utils.np_utils import to_categorical
import util.data_loader as dl
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import SpatialDropout1D
from tensorflow.keras.layers import Embedding


max_words = 3000


def generate_evaluation_file(review_ids, predictions_list: list, model_name):
    dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    with open("evaluations/eval_{}_{}.txt".format(model_name, dt_string), "w") as f:
        for i, prediction in enumerate(predictions_list):
            f.write("{} {}\n".format(review_ids[i], str(prediction).replace(".", ",")))


def get_mlp_model(vocab_size):
    model = Sequential()
    model.add(Embedding(vocab_size, 32, input_length=max_words))
    model.add(Flatten())
    model.add(Dense(250, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model, "models/mlp_model", "mlp"


def get_mlp_model_v2(vocab_size):
    model = Sequential()
    model.add(Embedding(vocab_size, 64, input_length=max_words))
    model.add(Flatten())
    model.add(Dense(250, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model, "models/mlp_model_v2", "mlp_v2"


def get_cnn_model(vocab_size):
    model = Sequential()
    model.add(Embedding(vocab_size, 32, input_length=max_words))
    model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(250, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model, "models/cnn_model", "cnn"


def get_cnn_model_v2(vocab_size):
    model = Sequential()
    model.add(Embedding(vocab_size, 64, input_length=max_words))
    model.add(Conv1D(filters=32, kernel_size=3, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(250, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model, "models/cnn_model_v2", "cnn_v2"


def main():
    # x = commentaires
    # y = notes

    max_comment = 10000

    # Load the train data
    df_train = dl.read_json_to_dataframe("corpus/preprocessed/train.json")
    df_train = df_train.head(max_comment)

    # Prepare the vocab
    comments = df_train.comment.values
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(comments)
    vocab_size = len(tokenizer.word_index) + 1

    # Encode the train
    x_train = tokenizer.texts_to_sequences(comments)
    x_train = pad_sequences(x_train, maxlen=max_words)
    y_train, y_list = df_train.note.factorize()

    # Load the dev data
    df_dev = dl.read_json_to_dataframe("corpus/preprocessed/dev.json")
    # df_dev = df_dev.head(max_comment)

    # Encode the dev
    x_dev = tokenizer.texts_to_sequences(df_dev.comment.values)
    x_dev = pad_sequences(x_dev, maxlen=max_words)
    y_dev = df_dev.note.factorize()[0]

    # Load the test
    df_test = dl.read_json_to_dataframe("corpus/preprocessed/test.json")
    # df_test = df_test.head(max_comment)

    # Encode the test
    x_test = tokenizer.texts_to_sequences(df_test.comment.values)
    x_test = pad_sequences(x_test, maxlen=max_words)

    # Reshape train
    # x_train = x_train.reshape(max_comment, max_words)

    # Reshape dev
    # x_dev = x_dev.reshape(max_comment, max_words)

    # Reshape test
    # x_test = x_test.reshape(max_comment, max_words)

    # To categorical
    y_train = to_categorical(y_train, 10)
    y_dev = to_categorical(y_dev, 10)

    # Get the model
    model, model_path, model_name = get_cnn_model(vocab_size)
    print(model.summary())

    # # Train the model
    # model.fit(x_train, y_train, validation_data=(x_dev, y_dev), epochs=5, batch_size=32, steps_per_epoch=len(x_train)//32, use_multiprocessing=True)
    # model.save(model_path)

    # Load the model
    model = keras.models.load_model("models/cnn_model")

    # Evaluate
    scores = model.evaluate(x_dev, y_dev, verbose=0)
    print("Accuracy: {}%".format(scores[1]*100))

    # Predict
    predictions = model.predict(x_test)
    generate_evaluation_file(df_test.review_id, [y_list[numpy.argmax(predictions[i])] for i in range(len(predictions))], model_name)


if __name__ == '__main__':
    main()
