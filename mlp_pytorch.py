import json
import os

from model.mlp import MLP
from util import data_loader as dl
import torch
import torch.nn as nn


def preprocess_data():
    df_dev = dl.read_json_to_dataframe("corpus/dev.json")
    df_dev = dl.preprocess_dataframe(df_dev)
    df_dev = dl.tokenize_dataframe(df_dev)
    dl.write_dataframe_to_json(df_dev, 'corpus/preprocessed/tokenized/dev.json')
    df_test = dl.read_json_to_dataframe("corpus/test.json")
    df_test = dl.preprocess_dataframe(df_test)
    df_test = dl.tokenize_dataframe(df_test)
    dl.write_dataframe_to_json(df_test, 'corpus/preprocessed/tokenized/test.json')
    df_train = dl.read_json_to_dataframe("corpus/train.json")
    df_train = dl.preprocess_dataframe(df_train)
    df_train = dl.tokenize_dataframe(df_train)
    dl.write_dataframe_to_json(df_train, 'corpus/preprocessed/tokenized/train.json')


def write_to_json(obj, filepath):
    with open(filepath, "w") as f:
        json.dump(obj, f, indent=4, ensure_ascii=False)


def word_to_index(words):
    com_idx = []
    # com_idx.append(vocab[word]) if word in vocab else com_idx.append(vocab["[unknown]"]) for word in words


def get_unique_words(comments):
    words = set()
    for com in comments:
        for word in com:
            words.add(word)
    return words


def create_vocab(words):
    vocab = {'[pad]': 0, '[unknown]': 1}
    for index, value in enumerate(words):
        vocab[value] = index + 2


def get_note_index(note):
    notes = {
        "0.5": 1,
        "1.0": 2,
        "1.5": 3,
        "2.0": 4,
        "2.5": 5,
        "3.0": 6,
        "3.5": 7,
        "4.0": 8,
        "4.5": 9,
        "5.0": 10
    }
    return notes[str(note)]


def main():
    df = dl.read_json_to_dataframe("data/dev.json")
    # comment = df_dev.pop("comment")
    # dataset = tf.data.Dataset.from_tensor_slices((df_dev.values, comment.values))

    with open("data/vocab.json", "r") as f:
        vocab = json.load(f)

    # write_to_json(vocab, "data/vocab.json")

    # df["comment_index"] = df["comment"].apply(lambda comment: [vocab[word] if word in vocab else vocab["[unknown]"] for word in comment])
    # dl.write_dataframe_to_json(df, "data/test.json")

    # df["note_index"] = df["note"].apply(lambda note: get_note_index(note))
    # dl.write_dataframe_to_json(df, "data/dev.json")

    # embeddings = nn.Embedding(len(vocab), 64, padding_idx=0)
    # for com in df["comment_index"]:
    #     com_length = len(com)
    #     for _ in range(3000 - com_length):
    #         com.append(vocab["[pad]"])
    #     tensor = torch.LongTensor(com)
    #     emb = embeddings(tensor)

    vocab_size = len(vocab)
    embeddings = nn.Embedding(vocab_size, 64, padding_idx=0)
    for index, row in df.iterrows():
        com_length = len(row["comment_index"])
        for _ in range(3000 - com_length):
            row["comment_index"].append(vocab["[pad]"])
        comment_tensor = torch.LongTensor(row["comment_index"])
        note_tensor = torch.LongTensor([row["note_index"]])
        comment_emb = embeddings(comment_tensor)




if __name__ == '__main__':
    main()




