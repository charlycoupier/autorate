# Analyse des données

## Préparation des xml

### Corriger les balises
Les fichiers xml sont à placer dans un dossier /corpus à la racine.
Ceux-ci sont erronés:
La balise <user_id> contient une coquille <used_id>.
Pour corriger ça :
```bash
sed -i.bak 's/used_id/user_id/g' train.xml
```
La commande crée un backup .xml.bak

### Corriger les caractères interdits

En xml, & > et < sont des caractères interdits. Tous sont présents dans les fichiers xml
Pour corriger ça 

Trouver les & qui ne sont pas déjà escaped
```
&(?!(amp;|gt;|lt;))
```
Les remplacer par 
```
&amp;
```

## Mouliner vers le json

Utiliser le script xmltojson.py

### Parser le bon fichier xml 

Ligne 4 du fichier xmltojson.py
```python
tree = et.parse('corpus/train.xml')
```
Remplacer le path par le path vers le bon fichier xml

### Ecrire dans le bon output

Ligne 34 du fichier xmltojson.py
```python
with open('mapping_train.json', 'a') as outfile:
```
Remplacer par le path vers le bon fichier json.

### Préparer les données

Pour préparer les données, on peut modifier cette moulinette afin qu'elle parse chaque commentaire pour préprocesser les datas qui seront visualisées dans la suite ELK.
On peut notamment retirer certains mots, on peut remplacer les versions escaped de & < et > par les caractères eux même, qui ne sont pas interdits en json. 

## Préparer le json pour importation vers Elastic search

Pour importer les données dans elastic search il faut des fichiers json (qui ne sont pas des fichiers json valides mais une suite d'instructions formatées en json) de moins de 100MB

### Découper le fichier json

```bash
split -d -a3 --bytes=99MB --additional-suffix=.json mapping_train.json train_ 
```

Attention, cette méthode découpe les fichiers sans regarder le contenu.
Il faut passer dans chaque fichier obtenu afin de corriger la coupure entre la fin d'un fichier et le début d'un autre.

### Importer le fichier dans ElasticSearch

```bash
curl -H 'Content-Type: application/x-ndjson' -XPOST 'localhost:9200/data_movie/_bulk?pretty' --data-binary @xaa.json
```

Remplacer localhost:9200 par l'hote et le port correspondant à la bdd cible

Remplacer xaa.json par le nom du fichier à importer


# Prédiction

Les fichiers commençant par "scitest" on servit pour le SVM

Les fichiers dans le dossier util contiennent des fonctions utilitaires pour le chargement de fichiers et le preprocessing.

Le fichier mlp_pytorch.py contient des essais d'utilisation de pytorch pour creer un réseau de neurones

Le fichier nn_keras contient nos réseaux de neurones MLP et CNN

Le dossier evaluations contient les fichiers générés en sorti de prédiction

Le dossier exploration contient les fichiers que nous avons fait pour l'exploration des données