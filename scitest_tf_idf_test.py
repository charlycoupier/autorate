from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn import svm
import xml.etree.ElementTree as et
from click import progressbar
from util.veronis_stop_words import veronis_sw
from datetime import datetime


class Dataset:
    def __init__(self):
        self.comment = []
        self.note = []
        self.review_id = []


def create_dataset(root_xml, is_test: bool = False):
    dataset = Dataset()
    with progressbar(list(root_xml)) as bar:
        for elt in bar:
            current_elt = {}
            note_ok = False
            comment_ok = False
            review_id_ok = False
            for info in list(elt):
                if not is_test and info.tag == "note":
                    if info.text != "" and info.text is not None:
                        note_ok = True
                        current_elt["note"] = info.text
                elif info.tag == "commentaire":
                    if info.text is None:
                        info.text = ""
                    comment_ok = True
                    current_elt["comment"] = info.text
                elif info.tag == "review_id":
                    if info.text != "" and info.text is not None:
                        review_id_ok = True
                        current_elt["review_id"] = info.text
            if (is_test or note_ok) and comment_ok and review_id_ok:
                dataset.comment.append(current_elt["comment"])
                dataset.review_id.append(current_elt["review_id"])
                if not is_test:
                    dataset.note.append(current_elt["note"])
    return dataset


def generate_evaluation_file(review_ids, predictions_list: list, max_iter):
    dt_string = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    with open("evaluations/eval_tf_idf_test_{}_{}.txt".format(max_iter, dt_string), "w") as f:
        for i, prediction in enumerate(predictions_list):
            f.write("{} {}\n".format(review_ids[i], prediction))


def main():
    max_iter = 500
    et.XMLParser(encoding="utf-8")
    tree = et.parse('corpus/train.xml')
    root = tree.getroot()
    dataset = create_dataset(root)
    print("train dataset created")

    classifier = Pipeline([
        ('vect', CountVectorizer(lowercase=False, stop_words=veronis_sw)),
        ('tfidf', TfidfTransformer(use_idf=True)),
        ('clsfier', svm.SVC(verbose=True, kernel='linear', max_iter=max_iter))
    ])
    classifier.fit(dataset.comment, dataset.note)
    print("classifier trained")

    test_tree = et.parse('corpus/test.xml')
    test_root = test_tree.getroot()
    dataset_test = create_dataset(test_root, True)
    print("Dataset train OK, start prediction")

    predicted_svm = classifier.predict(dataset_test.comment)

    generate_evaluation_file(dataset_test.review_id, predicted_svm, max_iter)


if __name__ == "__main__":
    main()
